# This is the original function used to calculate the difference between timesteps
# For higher indices the function returned incorrect and extremely high differences 

def calculate_difference(data):
    temp = data
    for i in range(data.shape[0]):
       if i > 0:
            data[i,0] = temp[i,0] - temp[(i-1),0]
            data[i,1] = temp[i,1] - temp[(i-1),1]
        else:
            data[i,0] = temp[i,0]
            data[i,1] = temp[i,1]
            print("First element", temp[i,0], temp[i,1])  
        i = i+1

   # get absolute values (-2 becomes 2)
    data = np.absolute(data)
    return data